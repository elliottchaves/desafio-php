<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class produto extends Model
{
    protected $table = 'produtos';
    protected $primaryKey = 'id';

    public function getAvailableProdutos()
    {
        $produtos = DB::table('produtos')
            ->select('produtos.id', 'produtos.nome','produtos.estoque', 'produtos.preco')
            ->get();

        return $produtos;
    }

    public function getSingleProdutoInfo($id)
    {
        $produto_info = DB::table('produtos')
            ->where('produtos.id',$id)
            ->select( 'produtos.nome','produtos.estoque', 'produtos.preco')
            ->get();

        return $produto_info;
    }
}
