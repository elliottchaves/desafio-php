<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class aluno extends Model
{
    protected $table = 'alunos';
    protected $primaryKey = 'id';

    public function getAvailableAlunos()
    {
        $alunos = DB::table('alunos')
            ->select('alunos.id','alunos.nome', 'alunos.data_nascimento'
            , 'alunos.turma_id', 'alunos.cep', 'alunos.endereco', 'alunos.bairro'
            , 'alunos.cidade', 'alunos.uf')
        ->get();
        return $alunos;
    }

    public function getSingleAlunoInfo($id)
    {
        $aluno_info = DB::table('alunos')
            ->where('alunos.id', $id)
            ->select('alunos.id','alunos.nome', 'alunos.data_nascimento'
                , 'alunos.turma_id', 'alunos.cep', 'alunos.endereco', 'alunos.bairro'
                , 'alunos.cidade', 'alunos.uf')
            ->get();

        return $aluno_info;
    }

}
