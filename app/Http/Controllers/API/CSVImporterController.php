<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\produto;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Support\Facades\DB;

class CSVImporterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $caminho_final = $request->file('file')->getRealPath();

            $linhas = array_map(function($v){return str_getcsv($v, ";");}, file($caminho_final));

            for($i = 1; $i < count($linhas); $i++ )
            {
                $produto = new produto();
                for($j = 0; $j < 7; $j++)
                {
                    switch ($j){
                        case 0:
                            $produto->nome = $linhas[$i][$j];
                            break;
                        case 1:
                            $produto->preco = str_replace(',','.',$linhas[$i][$j]);
                            break;
                        case 2:
                            $produto->estoque = $linhas[$i][$j];
                            $produto->save();
                            break;
                        case ($j == 3 && $linhas[$i][$j] == 'x'):
                            $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                                , [1,$produto->id]);
                            break;
                        case ($j == 4 && $linhas[$i][$j] == 'x'):
                            $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                                , [2,$produto->id]);
                            break;
                        case ($j == 5 && $linhas[$i][$j] == 'x'):
                            $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                                , [3,$produto->id]);
                            break;
                        case ($j == 6 && $linhas[$i][$j] == 'x'):
                            $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                                , [4,$produto->id]);
                            break;
                    }

                }
            }

            return json_encode('OK');

        } catch(\Exception $e)
        {
            return json_encode('FAIL');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
