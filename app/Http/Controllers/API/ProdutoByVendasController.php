<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\produto;
use App\venda;
use App\vendasItens;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProdutoByVendasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //getSingle/VendaInfo()
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getvendas($id_aluno, $id = 0)
    {
        $vendas = new venda();

        $retorno = $vendas->getSingleVendaInfo($id_aluno, $id);

        return response()->json($retorno);
    }

    public function postvendas(Request $request, $id,$id_aluno)
    {
        $produtos_qtd = $request->input('QtdxProd');

        $produtos_qtd_decoded = json_decode($produtos_qtd);

        $is_finalizada = $request->input('isFinalizada');

        $venda = new venda();

        $venda->aluno_id = $id_aluno;
        $venda->data = Carbon::now();
        $venda->finalizada = $is_finalizada;

        $venda->save();



        foreach ($produtos_qtd_decoded as $item)
        {
            $venda_itens = new vendasItens();

            $venda_itens->venda_id = $venda->id;
            $prod_id = explode('_', $item->id);
            $venda_itens->produto_id = $prod_id[1];
            $prod_info = produto::find($prod_id);

            $venda_itens->preco = $prod_info[0]->preco;
            $venda_itens->quantidade = $item->value;

           $venda_itens->save();
        }

        return response()->json(['OK']);

        //var_dump($produtos_qtd);
        //die();
    }

}
