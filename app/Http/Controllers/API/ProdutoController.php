<?php

namespace App\Http\Controllers\API;

use App\aluno;
use App\Http\Controllers\Controller;
use App\produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produto = new produto();
        $produto_list = $produto->getAvailableProdutos();
        return response()->json($produto_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req_data = $request->all();
        $produto = new produto();

        $produto->nome = $req_data['Nome'];
        $produto->estoque = $req_data['Estoque'];
        $produto->preco = $req_data['Preco'];

        $produto->save();

        return response()->json(['OK']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produto = new produto();

        $produto_info = $produto->getSingleProdutoInfo($id);

        return response()->json($produto_info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produto = produto::where('id',$id)->first();
        $produto_atualizado = $request->input();

        $produto->nome = $request->Nome;
        $produto->estoque = $request->Estoque;
        $produto->preco = $request->Preco;

        $produto->save();

        return response('',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
