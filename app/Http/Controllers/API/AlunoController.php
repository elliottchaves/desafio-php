<?php

namespace App\Http\Controllers\API;

use App\aluno;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aluno = new aluno();
        $aluno_list = $aluno->getAvailableAlunos();
        return response()->json($aluno_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req_data = $request->all();
        $aluno = new aluno();

        //var_dump($req_data);
        //die();

        $aluno->nome = $req_data['Nome'];
        $aluno->data_nascimento = $req_data['Nascimento'];
        $aluno->turma_id = $req_data['TurmaId'];
        $aluno->cep = $req_data['CEP'];
        $aluno->endereco = $req_data['Endereco'];
        $aluno->bairro = $req_data['Bairro'];
        $aluno->cidade = $req_data['Cidade'];
        $aluno->UF = $req_data['UF'];

        $aluno->save();

        return response()->json(['OK']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aluno = new aluno();

        $aluno_info = $aluno->getSingleAlunoInfo($id);
        return response()->json($aluno_info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aluno = aluno::where('id',$id)->first();
        $aluno_atualizado = $request->input();

        $aluno->nome = $request->Nome;
        $aluno->data_nascimento = $request->Nascimento;
        $aluno->turma_id = $request->TurmaId;
        $aluno->cep = $request->CEP;
        $aluno->endereco = $request->Endereco;
        $aluno->bairro = $request->Bairro;
        $aluno->cidade = $request->Cidade;
        $aluno->UF = $request->UF;

        $aluno->save();

        return response('',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
