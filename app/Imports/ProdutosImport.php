<?php

namespace App\Imports;

use App\produto;
use App\turma;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;

class ProdutosImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row)
        {
            $produto = new produto();

            $produto->nome = $row[0];
            $produto->preco = $row[1];
            $produto->estoque = $row[2];

            $produto->save();

            $turma = new turma();

            $todas_turmas = $turma->all();

            if($row[3] == 'x')
            {
                $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                    , [1,$produto->id]);
            }
            if($row[4] == 'x')
            {
                $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                    , [2,$produto->id]);
            }
            if($row[5] == 'x')
            {
                $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                    , [3,$produto->id]);
            }
            if($row[6] == 'x')
            {
                $insert = DB::insert('insert into turmas_material (turma_id,produto_id) values (?,?)'
                    , [4,$produto->id]);
            }

        }
    }

//    public function getCsvSettings(): array
//    {
//        return [
//            'delimiter' => ";"
//        ];
//    }

}
