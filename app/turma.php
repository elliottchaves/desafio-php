<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class turma extends Model
{
    protected $table = 'turmas';
    protected $primaryKey = 'id';

    public function getAvailableTurmas()
    {
        $turmas = DB::table('turmas')
            ->select('turmas.id', 'turmas.nome')
            ->get();
        return $turmas;
    }

    public function getSingleTurmaInfo($id)
    {
        $turma_info = DB::table('turmas')
            ->where('turma.id',$id)
            ->select('turma.nome')
            ->get();
        return $turma_info;
    }
}
