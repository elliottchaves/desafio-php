<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\This;

class venda extends Model
{
    protected $table = 'vendas';
    protected $primaryKey = 'id';

    public function getAvailableVendas()
    {
        $vendas = DB::table('vendas')
            ->join('alunos','vendas.aluno_id','=','alunos.id')
            ->select('vendas.id', 'alunos.nome as AlunoNome','alunos.id as IDaluno','vendas.finalizada')
            ->get();
        return $vendas;
    }

    public function getSingleVendaInfo($id,$id_aluno)
    {
        //$venda_info = DB::table('vendas')
         //   ->where('vendas.id',$id)
         //   ->select('vendas.')
//        $ache_isto = ['alunos.turma_id' => 'turmas_material.turma_id', 'alunos.id' => $id_aluno];
//        $venda_info_existentes = DB::table('produtos')
//            ->join('turmas_material','produtos.id','=', 'turmas_material.produto_id')
//            ->where($ache_isto)
//            ->rightJoin('vendas_itens','produtos.id','=','vendas_itens.produto_id')
//            ->rightJoin('vendas', function ($join) use ($id) {
//                $join->on('vendas_itens.venda_id', '=', 'vendas.id')
//                    ->where('vendas.id',$id);
//            })
//            //->rightJoin('vendas','vendas_items.venda_id', '=', 'vendas.id')
//            ->rightJoin('alunos','vendas_itens.aluno_id','=', 'alunos.id')
//            ->select('produtos.*','vendas_itens.preco as precoVendasItems','vendas_itens.quantidade as qtdVendasItems','vendas.finalizada')
//            ->get();

        $produtos_listagem = DB::table('produtos')
            ->select('produtos.id','produtos.nome','produtos.estoque','produtos.preco')
            ->join('turmas_material', 'produtos.id','=','turmas_material.produto_id')
            ->join('alunos','alunos.turma_id','=', 'turmas_material.turma_id')
            ->where('alunos.id','=',$id_aluno)
            ->get();

        return $produtos_listagem;

//        $venda_info_existentes = DB::table('produtos')
//            ->rightJoin('vendas_items','produtos.id','=','vendas_items.produto_id')
//            ->rightJoin('vendas', function ($join,$id) {
//                $join->on('vendas_items.venda_id', '=', 'vendas.id')
//                    ->where('vendas.id',$id);
//            })
//            ->rightJoin('vendas','vendas_items.venda_id', '=', 'vendas.id')
//            ->rightJoin('alunos','vendas_items.aluno_id','=', 'alunos.id')
//            ->select('produtos.*','vendas_items.preco as precoVendasItems','vendas_items.quantidade as qtdVendasItems','vendas.finalizada')
//            ->get();

            //->get();

        //return $venda_info_existentes;
    }
}
