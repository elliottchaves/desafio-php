<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vendasItens extends Model
{
    protected $table = 'vendas_itens';
    protected $primaryKey = 'id';
}
