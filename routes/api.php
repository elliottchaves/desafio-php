<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::apiresource('Aluno','API\AlunoController');

Route::apiresource('Turma','API\TurmaController');

Route::apiresource('Produto', 'API\ProdutoController');

Route::apiresource('CSVImport', 'API\CSVImporterController');

Route::apiresource('Venda','API\VendasController');

Route::get('ProdutoByVendas/{id}/aluno/{id_aluno}',['uses' => 'API\ProdutoByVendasController@getvendas'
    ,'produtoByVendas' ]);

Route::post('ProdutoByVendas/{id}/aluno/{id_aluno}',['uses' => 'API\ProdutoByVendasController@postvendas'
    ,'produtoByVendas' ]);

Route::put('ProdutoByVendas/{id}/aluno/{id_aluno}',['uses' => 'API\ProdutoByVendasController@putvendas'
    ,'produtoByVendas' ]);
