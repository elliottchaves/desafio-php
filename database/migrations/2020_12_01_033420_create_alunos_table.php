<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlunosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::disableForeignKeyConstraints();

        Schema::create('alunos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome',250);
            $table->date('data_nascimento');
            $table->bigInteger('turma_id')->unsigned();
            $table->index('turma_id');
            $table->foreign('turma_id')->references('id')->on('turmas');
            $table->bigInteger('cep');
            $table->string('endereco',250);
            $table->string('bairro',100);
            $table->string('cidade',100);
            $table->string('UF',2);
            $table->timestamps();
        });
       // Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alunos');
    }
}
