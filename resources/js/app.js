/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require ('./event-bus');
require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('alunosadd-component', require('./components/AlunosAddComponent').default);
Vue.component('alunoslist-component', require('./components/AlunosListComponent').default);
Vue.component('alunosform-component', require('./components/AlunosFormComponent').default);
Vue.component('produtosadd-component', require('./components/ProdutosAddComponent').default);
Vue.component('produtoslist-component', require('./components/ProdutosListComponent').default);
Vue.component('produtosform-component', require('./components/ProdutosFormComponent').default);
Vue.component('produtoscsvimport-component', require('./components/ProdutosCSVImportComponent').default);
Vue.component('produtoscsvformimport-component', require('./components/ProdutosCSVFormImportComponent').default);
Vue.component('vendaslist-component', require('./components/VendasListComponent').default);
Vue.component('vendasadd-component', require('./components/VendasAddComponent').default);
Vue.component('vendasform-component', require('./components/VendasFormComponent').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
