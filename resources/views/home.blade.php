@extends('welcome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" href="/home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/alunos">Gerenciar alunos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/produtos">Gerenciar produtos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/vendas">Gerenciar vendas</a>
                    </li>
                </ul>
            </div>
            <div class="col-10">
                @section('conteudo')
                    <p>Conteudo do home</p>
                @show
            </div>
        </div>
    </div>
@endsection
