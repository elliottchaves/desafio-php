@extends('home')

@section('conteudo')
    <h1>Gerenciamento de produtos</h1>
    <produtosadd-component></produtosadd-component>
    <produtosform-component></produtosform-component>
    <produtoslist-component></produtoslist-component>
    <produtoscsvformimport-component></produtoscsvformimport-component>
@endsection
